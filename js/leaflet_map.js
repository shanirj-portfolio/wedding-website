// code drives map, from LeafletJS

var map = L.map('map').setView([45.51786005543847, -122.65927622559818], 16);

var marker = L.marker([45.51786005543847, -122.65927622559818]).addTo(map);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(map);

marker.bindPopup("<b>THE EVERGREEN</b><br>618 SE Alder ST<br>Portland, OR 97214").openPopup();