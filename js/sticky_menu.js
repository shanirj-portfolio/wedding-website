window.onscroll = function() {makeMenuStickToTop()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function makeMenuStickToTop() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

// Address click should open map app based on device

// $(document).on('click','.map-link',function() {
//   var address = $(this).html();
//   address = $('<div/>')
//     .html(address)
//     .text() // strip tags
//     .replace(/\s\s+/g, " "); // remove spaces
//   address = encodeURIComponent(address);
//   if ((navigator.platform.indexOf('iPhone') != -1) || (navigator.platform.indexOf('iPad') != -1) || (navigator.platform.indexOf('iPod') != -1)){/* if we're on iOS, open in Apple Maps */
//       window.open('http://maps.apple.com/?q=' + address);
//   } else { /* else use Google */
//       window.open('https://maps.google.com/maps?q=' + address);
//   }
// });